
# ADM Engine AMQP worker

The purpose of this worker is to integrate the `adm_engine` tool easily into a message broker environment.
It is based on the [rs_mcai_worker_sdk](https://gitlab.com/media-cloud-ai/sdks/rs_mcai_worker_sdk) library and Rust FFI.

## Dependencies

The `cargo build` command triggers automatically the [adm_engine](./adm_engine) submodule library compilation.
In order to complete this compilation successfully, the [adm_engine](./adm_engine) library dependencies must be accessible
to the compiler and linker.
So you may have to:

 - add to the `LD_LIBRARY_PATH` environment variable, the paths to:
     - libadm.a
     - libear.a

 - add to the `ADM_INCLUDE_PATH`, `EAR_INCLUDE_PATH` and `BW64_INCLUDE_PATH` environment variable, respectively the paths to:
     - [libadm](https://github.com/IRT-Open-Source/libadm/) headers
     - [libear](https://github.com/ebu/libear) headers
     - [libbw64](https://github.com/IRT-Open-Source/libbw64) headers

## Usage

The worker can handle AMQP message under JSON format. Here are some usage examples:

* Dumping BW64/ADM file info:
   ```json
   {
     "job_id": 123,
     "parameters": [
       {
         "id": "source_path",
         "type": "string",
         "value": "/path/to/bw64_adm.wav"
       }
     ]
   }
   ```


* Rendering ADM:
   ```json
   {
     "job_id": 123,
     "parameters": [
       {
         "id": "source_path",
         "type": "string",
         "value": "/path/to/bw64_adm.wav"
       },
       {
         "id": "destination_path",
         "type": "string",
         "value": "/path/to/output/directory"
       }
     ]
   }
   ```


* Rendering specified ADM element:
   ```json
   {
     "job_id": 123,
     "parameters": [
       {
         "id": "source_path",
         "type": "string",
         "value": "/path/to/bw64_adm.wav"
       },
       {
         "id": "destination_path",
         "type": "string",
         "value": "/path/to/output/directory"
       },
       {
         "id": "element_id",
         "type": "string",
         "value": "APR_1002"
       }
     ]
   }
   ```


* Rendering ADM, applying gains to elements:
   ```json
   {
     "job_id": 123,
     "parameters": [
       {
         "id": "source_path",
         "type": "string",
         "value": "/path/to/bw64_adm.wav"
       },
       {
         "id": "destination_path",
         "type": "string",
         "value": "/path/to/output/directory"
       },
       {
         "id": "gain_mapping",
         "type": "array_of_strings",
         "value": [
           "AO_1001=-4.0"
         ]
       }
     ]
   }
   ```


* Rendering a specified ADM element, applying gains on others:
   ```json
   {
     "job_id": 123,
     "parameters": [
       {
         "id": "source_path",
         "type": "string",
         "value": "/path/to/bw64_adm.wav"
       },
       {
         "id": "destination_path",
         "type": "string",
         "value": "/path/to/output/directory"
       },
       {
         "id": "element_id",
         "type": "string",
         "value": "APR_1002"
       },
       {
         "id": "gain_mapping",
         "type": "array_of_strings",
         "value": [
           "ACO_1003=3.0",
           "ACO_1004=-6.0"
         ]
       }
     ]
   }
   ```
