#[macro_use]
extern crate serde_derive;

use mcai_worker_sdk::{job::JobResult, prelude::*, MessageEvent};

use crate::adm_engine::{dump_adm_content, process_adm_rendering};

mod adm_engine;

pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

#[derive(Debug, Default)]
struct AdmEngineEvent {}

#[derive(Debug, Clone, Deserialize, JsonSchema)]
pub struct AdmEngineWorkerParameters {
  /// BW64 ADM source file path
  source_path: String,
  /// Rendering destination directory
  destination_path: Option<String>,
  /// ID of the element to render
  element_id: Option<String>,
  /// Gain per element
  gain_mapping: Option<Vec<String>>,
}

impl MessageEvent<AdmEngineWorkerParameters> for AdmEngineEvent {
  fn get_name(&self) -> String {
    "ADM Engine worker".to_string()
  }

  fn get_short_description(&self) -> String {
    "Processes BW64/ADM audio file rendering.".to_string()
  }

  fn get_description(&self) -> String {
    r#"This worker processes ADM rendering from specified BW64/ADM audio file."#.to_string()
  }

  fn get_version(&self) -> Version {
    Version::parse(built_info::PKG_VERSION).expect("unable to locate Package version")
  }

  fn process(
    &self,
    _channel: Option<McaiChannel>,
    parameters: AdmEngineWorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    info!(target: &job_result.get_str_job_id(), "Start ADM Engine Job");

    if let Some(destination_path) = &parameters.destination_path {
      process_adm_rendering(
        &parameters.source_path,
        destination_path,
        parameters.element_id,
        parameters.gain_mapping,
        job_result,
      )
    } else {
      dump_adm_content(&parameters.source_path, job_result)
    }
  }
}

fn main() {
  let worker = AdmEngineEvent::default();
  start_worker(worker);
}
