use mcai_worker_sdk::prelude::{debug, info, JobResult, JobStatus, MessageError, Result};
use std::collections::HashMap;
use std::ffi::CString;
use std::os::raw::{c_char, c_float, c_int, c_uint, c_void};
use std::str::FromStr;

macro_rules! get_c_string {
  ($name:expr) => {
    if $name.is_null() {
      "".to_string()
    } else {
      std::str::from_utf8_unchecked(std::ffi::CStr::from_ptr($name).to_bytes()).to_string()
    }
  };
}

#[repr(C)]
struct ElementGainEntry {
  element_id: *const c_char,
  gain: c_float,
}

#[link(name = "adm", kind = "static")]
#[link(name = "ear", kind = "static")]
#[link(name = "admengine", kind = "static")]
extern "C" {
  fn dump_bw64_adm(source_path: *const c_char, output: &*const c_char) -> c_int;

  fn process_rendering(
    source_path: *const c_char,
    destination_path: *const c_char,
    element_id: *const c_char,
    gain_mapping: *const ElementGainEntry,
    gain_mapping_size: c_uint,
  ) -> c_int;
}

fn to_c_string(string: &str) -> Result<CString> {
  CString::new(string).map_err(|error| {
    MessageError::RuntimeError(format!("Cannot convert {:?} to CString: {}", string, error))
  })
}

fn free_pointer(pointer: *mut c_void) {
  if !pointer.is_null() {
    debug!("Free pointer: {:?}", pointer);
    unsafe {
      libc::free(pointer);
    }
  }
}

pub(crate) fn dump_adm_content(source_path: &str, job_result: JobResult) -> Result<JobResult> {
  let source_path_c_str = to_c_string(source_path)?;
  let source_path_ptr = source_path_c_str.as_ptr();

  let dump_output_ptr = std::ptr::null();

  let returned_code = unsafe { dump_bw64_adm(source_path_ptr, &dump_output_ptr) };
  if returned_code != 0 {
    free_pointer(dump_output_ptr as *mut c_void);
    return Err(MessageError::ProcessingError(
      job_result.with_message("BW64 file dump failed"),
    ));
  }

  if dump_output_ptr.is_null() {
    return Err(MessageError::ProcessingError(
      job_result.with_message("Null dump output ptr"),
    ));
  }
  let from_c_string = unsafe { get_c_string!(dump_output_ptr) };

  free_pointer(dump_output_ptr as *mut c_void);
  info!(target: &job_result.get_str_job_id(), "'{}' ADM content:\n{}", source_path, from_c_string);

  Ok(job_result.with_status(JobStatus::Completed))
}

pub(crate) fn process_adm_rendering(
  source_path: &str,
  destination_path: &str,
  element_id: Option<String>,
  gain_mapping: Option<Vec<String>>,
  job_result: JobResult,
) -> Result<JobResult> {
  let source_path_c_string = to_c_string(source_path)?;
  let source_path_ptr = source_path_c_string.as_ptr();

  let destination_path = to_c_string(destination_path)?;
  let destination_path_ptr = destination_path.as_ptr();

  // Element to render
  let element_id_c_string;
  let mut element_id_ptr = std::ptr::null();

  if let Some(element_id) = &element_id {
    info!("Set element to render: {}", element_id);
    element_id_c_string = to_c_string(element_id)?;
    element_id_ptr = element_id_c_string.as_ptr();
  }

  // Gain mapping
  let mut c_string_gain_mapping = HashMap::new();

  if let Some(gain_mapping) = &gain_mapping {
    for gain_per_element in gain_mapping {
      let split = gain_per_element.split('=').collect::<Vec<&str>>();

      let element_id = split.get(0).ok_or_else(|| {
        MessageError::ParameterValueError(format!(
          "Invalid gain mapping format: cannot access element ID from {}",
          gain_per_element
        ))
      })?;

      let gain = split.get(1).ok_or_else(|| {
        MessageError::ParameterValueError(format!(
          "Invalid gain mapping format: cannot access element gain from {}",
          gain_per_element
        ))
      })?;

      let gain = f32::from_str(gain).map_err(|error| {
        MessageError::ParameterValueError(format!("Gain conversion failed: {}", error))
      })?;

      info!("Apply gain to element {}: {} dB", element_id, gain);

      let element_id_c_string = to_c_string(element_id)?;
      c_string_gain_mapping.insert(element_id_c_string, gain);
    }
  }
  let gain_mapping_entries: Vec<ElementGainEntry> = c_string_gain_mapping
    .iter()
    .map(|(e, g)| ElementGainEntry {
      element_id: e.as_ptr(),
      gain: *g,
    })
    .collect();

  let gain_mapping_ptr = gain_mapping_entries.as_ptr();

  let returned_code = unsafe {
    process_rendering(
      source_path_ptr,
      destination_path_ptr,
      element_id_ptr,
      gain_mapping_ptr,
      gain_mapping_entries.len() as c_uint,
    )
  };

  if returned_code != 0 {
    return Err(MessageError::ProcessingError(
      job_result.with_message("Rendering process failed"),
    ));
  }

  Ok(job_result.with_status(JobStatus::Completed))
}
